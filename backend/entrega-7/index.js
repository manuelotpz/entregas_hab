/**
 * El objetivo de este ejercicio es implementar una aplicación de consola
 * para calcular la cuota mensual de un préstamo. Para ello, la aplicación debe:
 *     - usar una librería para realizar los cálculos necesarios (buscad en npmjs
 *       por las palabras loan, mortgage o payoff)
 *     - recibir por línea de comandos los argumentos necesarios para realizar los
 *       cálculos. Para esto podéis usar directamente la API de NodeJS (process.argv)
 *       o bien usar una librería como commander o yargs 
 *     - el resultado debe aparecer en la consola
 * 
 */

const Loan = require('loan')

const monto = process.argv[2];
const aporte = process.argv[3];
const plazos = process.argv[4];

resta = parseInt(process.argv[2] - process.argv[3])

if (process.argv.length < 5) {
  console.log('Faltan parámetros, recuerde pedir en este orden, primero lo que necesita, luego su amortización inicial y a continuación plazos en meses, interés fijo del 7%');
  return
}

if (resta <= 0) {
  console.log('Parámetros incorrectos');
  return
}

var loan = new Loan({
  type: 'annuity',
  pay_every: 'month',
  principal: resta,
  interest_rate: 0.07,
  instalments: plazos,
});

console.log(`Su mensualidad será de ${(loan.amortize().to_pay).toFixed(2)} euros.`)

