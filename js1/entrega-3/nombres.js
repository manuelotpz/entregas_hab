/**
 * Entregable semana 2
 * 
 * Escribe el código necesario para decidir en qué
 * fotografías sale Pablo. Como resultado se debe
 * obtener un array de strings con los nombres de las
 * fotografías.
 *  
 */

const photos = [
    {
        name: 'Cumpleaños de 13',
        people: ['Maria', 'Pablo']
    },
    {
        name: 'Fiesta en la playa',
        people: ['Pablo', 'Marcos']
    },
    {
        name: 'Graduación',
        people: ['Maria', 'Lorenzo']
    },
]

const pabloSaleEn = []


// Solución con for
for (let photo of photos) {
    for (let Pablo of photo.people) {
        if (Pablo === 'Pablo') {
            pabloSaleEn.push(photo.name)
        }
    }
}
console.log(pabloSaleEn)


// Solución con map
const Pablo = photos
    .map(function (photo) {
        for (let Pablo of photo.people) {
            if (Pablo === 'Pablo') {
                return pabloSaleEn.push(photo.name)
            }
        }
    }
    )
console.log(pabloSaleEn)


// Solución con arrow
const Pablo = photos
    .map(photo => {
        for (let Pablo of photo.people) {
            if (Pablo === 'Pablo') {
                pabloSaleEn.push(photo.name)
            }
        }
    }
    )
console.log(pabloSaleEn)